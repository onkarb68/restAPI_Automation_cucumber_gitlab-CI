# Getting started with REST API Automation testing with Cucumber , Extent Report & Rest Assured

## Automation setup to get started
	Git: Simply clone below project and import in Eclipse. voila!! And you are great to go.. 

    git clone https://gitlab.com/onkarb68/restAPI_Automation_cucumber_gitlab-CI.git
    cd restAPI_Automation_cucumber_gitlab-CI

# Project overview

	This project gives you a basic project setup, along with some sample tests and supporting classes. 
	The starter project shows , how one can validate, automate any API or feature end to end using Rest Assured library.

## Test Scenarios(Positive and Negative) Covered https://developers.themoviedb.org Web application to test and automate API. Details test step can be seen in cucumber feature file.
	If we would have access to Jira application to author test scenario, test cases , create test cycle to execution. This sounds like a great test artefact one can have. Here, we have used Extent report as test automation artifact for reference. 
	
	1. Verify user is able to generate request token & access token
	2. Verify user is able to create the list using POST call
	3. Verify user is able to update the list using PUT call
	4. Verify user is able to add the items into the list using POST call
	5. Verify user is able to clear the items into the list using GET call
	6. [Negative Tc]User should not able to create the list using POST call for incorrect access token
	7. [Negative Tc]User should not able to update the items for unathourized list
	8. [Negative Tc]User should not be able to see added items in the unathourized list
	9. [Negative Tc]User should not be able to clear items in the unathourized list

## 1. How to write reusable core API Automation framework.
 
	Location: src/test/java/Utility/RestAssuredAPIFramework
			........->This class file is having all the CRUD API http calls wrapped under single umbrella. The advantage of doing this is easy to maintain single file VS multiple files.
			e.g. In future, if project business demands to automate API using latest version of rest assured, we only need to change this single java file rest all files will remain untouched. This kind of beauty and autonomy this framework brings on the table. 

## 2.  How & Where to write feature files?
	Location:  ./Features/Movie_Database_API
	
	Gherkin uses a set of special keywords to give structure and meaning to executable specifications. 
	Keywords:
		The primary keywords are:

			1.Feature
			2.Given, When, Then, And, But for steps (or *)
			3.Background
			4.Scenario Outline (or Scenario Template)
			5.Examples (or Scenarios)
			
		There are a few secondary keywords as well:

			1.""" (Doc Strings)
			2.| (Data Tables)
			3.@ (Tags)
			4.# (Comments)
	Example:
		Feature: Verify various Create, Update , Add , Get , Clear list APIs of themoviedb
		
		 Scenario: Verify user is able to create the list using POST call
			Given User is able to create the list using POST call
			When User validates that list is created using API Response
			Then User should be able to see the created list on list page
			But [Negative Tc]User should not able to create the list using POST call for incorrect access token
	
## 3.  Where to write step definition? 
		The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
	
	Location: src/test/java/StepDefination/MovieDatabaseAPIStep
		
## 4.  How to group reusable feature API specific components?
	Location: src/test/java/APIObject/MovieDBAPI
		The actual REST calls are performed using RestAssured in the action classes, like `MovieDBAPI` here. 
	These methods use internally consuming core http calls from RestAssured Utility. This will have reusable API specific components or methods which can be consumed anywhere, which helps to make Automation code more modular.
	E.g. Lets consider "createAccessToken" method. In case, its implementation gets changed in application. We need to only change in single file, so your entire automation code will not get impacted. This is the great modularity it provides.
	
## 5. How to perform data driven testing in QA framework?	
	Location: src/test/java/DataCenter/MovieDBData
	
	This file will have the test data like payload, end points URI to perform data driven testing. So if you want to test API with other data set, you can always do it without altering other framework.
	The reason to keep end points URI in this file is that if any end points gets changed, will only change in single file rather changing all files where it got consumed.
	
	So very important point , Our QA framework should always be modular, less time consuming to adopt new testing requirements & should be easily able to upgrade to latest versions of various libraries got consumed in QA frameowrk. 
	
	We also used datatables for data driven testing in cucumber as following?
	
	Background: Verify user is able to generate request token & access token
    Given User Generates the new request Token using POST Call
    When User approves the token by logging in to TMDb website using Username and Password
      | onkarb68 | Tmdb@123 |
    Then Generates the access Token using an approved request token over the POST Call
	
	@When("^User approves the token by logging in to TMDb website using Username and Password$")
	public void useApprovesToken(DataTable credentials) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		List<List<String>> rows = credentials.asLists(String.class);
		
		How to access data?
		>> rows.get(0).get(0)
	}

## 6. How to generate Test execution  artifacts , reports?
	
	This is again an awesome plugin that is built on Extent Report specially for Cucumber along with RestAssured API.
	
		Location:  ./ExtentReports/cucumber-reports/report.html
		
## 7. How to generate test execution log?
		Implemented Logger(log4j library) for quick debugging and RCA purpose
		Location:Log/Application.log

## 8.How to implement Gitlab CI Pipeline?
	
	GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever developer pushes code to application.
	So whenever, Git push events happens, implicitly gitlab CI pipeline job gets triggered which would decide Pull request merge based on it pipelines execution status. 

	.gitlab-ci.yml
	
		demo_job_1:
     tags:
       - ci
     script:
      - mvn clean verify
## 9.Have worked on following pointers in this assignment :

	1. Have referred https://developers.themoviedb.org/  for API Automation as mentioned in doc
	2. Cucumber as BDD test framework
	3. Developed scratch modular QA Automation framework using Cucumber +  Rest Assured + Extent Report +  Log4j logger + Gitlab CI pipeline
	4. Created reusable RestAssuredFramework which is so generic that can be plugged and played to automate any projects API
	5. Build the Gitlab CI pipeline successfully(Refer attachment) which is Automating the endpoints inside the CI pipeline to test the service before PR gets merged
	6. Implemented Logger(log4j library) for quick debugging and RCA purpose
	7. Implemented the Extent report as QA reporting mechanism into the framework
	8. Data driven testing using Data table 
	9. Covered the possible positive and negative scenarios as described in readme file
	10. Done the code indentation for better readability, reusability & maintainability of the code
	11. Updated README file

## 10. What else Future Enhancement can we embed to make it a full fledged API Automation framework?
	1. Database automation using JDBC socket : It will help us to validate equivalent changes done when any API into the database, by which it will be able to perform end to end API testing.
	e.g. For UPDATE API, though we would get 200 status code, along with that will also validate the database table to check the actual updation happening in the database using JDBC socket. 
	2. Build Sanity API Automation suite to get quick health of production server
	3. To build Performance test Automation for API execution using jmeter
	4. Perform security testing of API's
	5. More intuitive Extent Report
	6. Parallel testing to speed up test execution time
	