Feature: Verify various Create, Update , Add , Get , Clear list APIs of themoviedb

	#Creating fresh request token and access before every test run so that we can make sure all API's are up and running with latest tokens. 
  Background: Verify user is able to generate request token & access token
    Given User Generates the new request Token using POST Call
    When User approves the token by logging in to TMDb website using Username and Password
      | onkarb68 | Tmdb@123 |
    Then Generates the access Token using an approved request token over the POST Call

  Scenario: Verify user is able to create the list using POST call
    Given User is able to create the list using POST call
    When User validates that list is created using API Response
    Then User should be able to see the created list on list page
    But [Negative Tc]User should not able to create the list using POST call for incorrect access token
	 #List is getting deleted after test run in Cucumber Hooks using DELETE call
  
  Scenario: Verify user is able to update the list using PUT call
    Given User is able to create the list using POST call
    When User updates items in list using PUT call
    Then User should be able to see the updated items in list
    But [Negative Tc]User should not able to update the items for unauthorized list
	#List is getting deleted after test run using Cucumber Hooks using DELETE call
  
  Scenario: Verify user is able to add the items into the list using POST call
    Given User is able to create the list using POST call
    When User adds the items into the created list using POST call
    Then User should be able to see added items in the list
    But [Negative Tc]User should not be able to see added items in the unauthorized list
    #List is getting deleted after test run using Cucumber Hooks using DELETE call

    Scenario: Verify user is able to clear the items into the list using GET call
    Given User is able to create the list using POST call
    When User clears the items into the created list using GET call
    Then User should be able to verify cleared items in the list
    But [Negative Tc]User should not be able to clear items in the unauthorized list
    #List is getting deleted after test run using Cucumber Hooks using DELETE call