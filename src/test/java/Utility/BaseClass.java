package Utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass {
	static Logger logger = Logger.getLogger("BaseClass");
	public static WebDriver driver;
	public static boolean bResult;
	public static Properties prop;

	public BaseClass(WebDriver driver) {
		BaseClass.driver = driver;
		BaseClass.bResult = true;
	}

	// This method wait till entire DOM gets loaded completely
	public void waitForContent() {
		ExpectedCondition<Boolean> loaded = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(@Nonnull WebDriver driver) {
				String result = "return ((document.readyState === 'complete') && ($.active == 0))";
				return (Boolean) ((JavascriptExecutor) driver).executeScript(result);
			}
		};
		try {
			new WebDriverWait(BaseClass.driver, 100).until(loaded);
		} catch (WebDriverException e) {
			loaded = new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(@Nonnull WebDriver driver) {
					String result = "return ((document.readyState === 'complete'))";
					return (Boolean) ((JavascriptExecutor) driver).executeScript(result);
				}
			};
			new WebDriverWait(BaseClass.driver, 100).until(loaded);
		}
	}

	public WebElement getElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(BaseClass.driver, 150);
		waitForContent();
		wait.until(ExpectedConditions.visibilityOf(element));
		WebElement ele = wait.until(ExpectedConditions.elementToBeClickable(element));
		return ele;
	}

	// This method to be used for the element which frequently gets changed in DOM
	// and gives StaleElementReferenceException
	public boolean retryingClick(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 20) {
			try {
				waitForContent();
				retryingFindElement(ele);
				ele.click();
				result = true;
				break;
			} catch (NoSuchElementException e) {

				logger.info("Refinding the element as its throwing NoSuchElementException");

			} catch (ElementClickInterceptedException e) {

				logger.info("Refinding the element as its throwing ElementClickInterceptedException");

			} catch (StaleElementReferenceException e) {

				logger.info("Refinding the element as its throwing StaleElementReferenceException");

			} catch (TimeoutException e) {

				logger.info("Refinding the element as its throwing TimeoutException");

			} catch (Exception e) {

				logger.info("Refinding the element as its throwing exception");
			}
			waitForContent();
			attempts++;
		}
		return result;
	}

	// This method to be used for the element which frequently gets changed in DOM
	// and gives NoSuchElementException
	public boolean retryingFindElement(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 20) {
			try {
				waitForContent();
				ele.isDisplayed();
				result = true;
				break;
			} catch (NoSuchElementException e) {

				logger.info("Refinding the element as its throwing NoSuchElementException");

			} catch (ElementClickInterceptedException e) {

				logger.info("Refinding the element as its throwing ElementClickInterceptedException");

			} catch (StaleElementReferenceException e) {

				logger.info("Refinding the element as its throwing StaleElementReferenceException");

			} catch (TimeoutException e) {

				logger.info("Refinding the element as its throwing TimeoutException");

			} catch (Exception e) {

				logger.info("Refinding the element as its throwing exception");
			}
			waitForContent();
			attempts++;
		}
		return result;
	}

	// This method will instantiate browser
	public void openBrowser(String URI) throws IOException {
		prop = new Properties();
		FileInputStream fis = new FileInputStream("src//main//resources//webservices.properties");
		prop.load(fis);
		String url = prop.getProperty("url");
		String Browsername = prop.getProperty("browser");
		logger.info("Fetching the Application URL from File --> " + " " + url + " " + "Opening the Url");

		if (Browsername.equals("chrome")) {
			logger.info("Called openBrowser");
			ChromeOptions options = new ChromeOptions();

			System.setProperty("webdriver.chrome.driver", "driver//chromedriver.exe");
			options.setPageLoadStrategy(PageLoadStrategy.NONE);
			options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			driver = new ChromeDriver(options);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();

			// Runtime.getRuntime().exec("AutoIT_Exe//AutoIT_Login.exe");

			driver.get(prop.getProperty("url") + URI);
			// driver.get("http://www.extentreports.com/docs/versions/5/java/index.html");

		} else if (Browsername.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", "driver//geckodriver.exe");
			driver = new FirefoxDriver();
			// driver.manage().deleteAllCookies();
			driver.get(prop.getProperty("url"));
			driver.manage().window().maximize();
		}
	}

	public void tearDown() throws IOException {
		driver.close();
		driver.quit();

	}
}
