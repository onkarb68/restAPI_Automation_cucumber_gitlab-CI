package Utility;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class RestAssuredAPIFramework {
	Logger logger = Logger.getLogger("RestAssuredAPIFramework");

	// Method is used to get the Server Endpoint URI
	@SuppressWarnings("unused")
	public String getServerAPIEndpoint() {

		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src//main//resources//webservices.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("API_ENDPOINT is: " + prop.getProperty("API_ENDPOINT"));
		return prop.getProperty("API_ENDPOINT");

	}

	// Method is used to get the response with request headers and parameters
	public Response getCall(String apiEndPoint, Map<String, ?> reqHeaders) {

		RestAssured.baseURI = getServerAPIEndpoint();
		logger.info("Base URI is: " + RestAssured.baseURI);
		RequestSpecification httpRequest = RestAssured.given();
		// Response response = httpRequest.get(apiEndPoint);
		Response response = httpRequest.headers(reqHeaders).get(apiEndPoint);
		return response;

	}

	// Method is used to trigger the POST call
	public Response postCall(Map<String, String> requestHeader, String requestBody, String postAPI) {
		RestAssured.baseURI = getServerAPIEndpoint();

		Response response = given().headers(requestHeader).and().body(requestBody).when().post(postAPI).then().extract()
				.response();

		return response;

	}

	// Method is used to trigger the PUT call
	public Response putCall(Map<String, String> requestHeader, String requestBody, String postAPI) {
		RestAssured.baseURI = getServerAPIEndpoint();

		Response response = given().headers(requestHeader).and().body(requestBody).when().put(postAPI).then().extract()
				.response();

		return response;

	}

	// Method is used to trigger the DELETE call
	public Response deleteCall(Map<String, ?> requestHeader, String deleteAPI) {

		RestAssured.baseURI = deleteAPI;
		logger.info("deleteAPI  " + deleteAPI);
		Response response = given().headers(requestHeader).when().delete(deleteAPI).then().extract().response();

		return response;

	}

	// Method is used to get the response code
	public int getResponseStatus(String apiEndPoint) {
		int statusCode = given().when().get(apiEndPoint).getStatusCode();
		logger.info("Status code is: " + statusCode);
		return statusCode;
	}

	// Method is used to get the response
	public Response getResponse(String apiEndPoint) {

		RestAssured.baseURI = getServerAPIEndpoint();
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get(apiEndPoint);
		return response;
	}

}
