package APIObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.google.gson.JsonObject;

import DataCenter.MovieDBData;
import Utility.BaseClass;
import Utility.RestAssuredAPIFramework;
import io.restassured.response.Response;

public class MovieDBAPI extends BaseClass {

	Logger logger = Logger.getLogger("MovieDBAPI");

	static Response response;
	RestAssuredAPIFramework restAssuredAPIFramework = new RestAssuredAPIFramework();
	MovieDBData movieDBData = new MovieDBData();

	public MovieDBAPI(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	// Configure multiple request headers
	public Map<String, String> configureRequestHeaders(String tokenValue) {
		String bearerToken;
		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src//main//resources//webservices.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (tokenValue.equalsIgnoreCase("propertiesFile")) {
			bearerToken = prop.getProperty("BEARER_TOKEN");
		} else {
			bearerToken = tokenValue;
		}

		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("authorization", "Bearer " + bearerToken);
		requestHeaders.put("content-type", "application/json;charset=utf-8");

		return requestHeaders;
	}
	
	//This method creates the request token as a payload for access token creation
	public Response createRequestToken() {

		movieDBData = new MovieDBData();
		logger.info("headers  " + configureRequestHeaders("propertiesFile"));
		logger.info("createRequestToken URI is: " + movieDBData.createRequestTokenURI);
		response = restAssuredAPIFramework.postCall(configureRequestHeaders("propertiesFile"), "",
				movieDBData.createRequestTokenURI);
		return response;
	}
	
	//This method creates access token for API calls
	public Response createAccessToken(String requestToken) {

		logger.info("Request headers are " + configureRequestHeaders("propertiesFile"));

		logger.info("createAccessToken URI is: " + movieDBData.createAccessTokenURI);
		// Create new JSON Object
		JsonObject jsonBody = new JsonObject();
		jsonBody.addProperty("request_token", requestToken);

		response = restAssuredAPIFramework.postCall(configureRequestHeaders("propertiesFile"), jsonBody.toString(),
				movieDBData.createAccessTokenURI);
		return response;
	}
	
	//This method creates the list 
	public Response createList(String accessToken) {

		logger.info("Create list URI is: " + movieDBData.createListURI);
		response = restAssuredAPIFramework.postCall(configureRequestHeaders(accessToken),
				movieDBData.getPayloadForCreateList().toString(), movieDBData.createListURI);
		return response;

	}
	
	//This method deletes the provided list
	public Response deleteList(String accessToken, String listId) {

		String deleteListURI = movieDBData.deleteListURI(listId);
		logger.info("Delete list URI is: " + deleteListURI);
		response = restAssuredAPIFramework.deleteCall(configureRequestHeaders(accessToken), deleteListURI);
		return response;

	}
	
	//This method adds items into the provided list
	public Response addItemsInList(String accessToken, String listId, String payload) {

		logger.info("Add item URI is: " + movieDBData.getAddItemsURI(listId));
		response = restAssuredAPIFramework.postCall(configureRequestHeaders(accessToken), payload,
				movieDBData.getAddItemsURI(listId));
		return response;

	}

	//This method updates items into the provided list
	public Response updateItemsInList(String accessToken, String listId) {

		MovieDBData movieDBData = new MovieDBData();
		logger.info("Update item URI is: " + movieDBData.getUpdateItemsURI(listId));
		response = restAssuredAPIFramework.putCall(configureRequestHeaders(accessToken), movieDBData.payload,
				movieDBData.getUpdateItemsURI(listId));
		return response;

	}

	//This method clears the items from the provided list
	public Response clearItemsInList(String accessToken, String listId) {

		logger.info("Clear item URI is: " + movieDBData.getClearItemsURI(listId));
		response = restAssuredAPIFramework.getCall(movieDBData.getClearItemsURI(listId),
				configureRequestHeaders(accessToken));
		return response;

	}
}
