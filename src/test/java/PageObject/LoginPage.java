package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.BaseClass;
import io.restassured.response.Response;

public class LoginPage extends BaseClass{

	static Logger logger = Logger.getLogger("LoginPage");

	static Response response;
	
	public LoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(how = How.XPATH, using = "//a[@href=\"/login\"]")
	public static WebElement login;

	@FindBy(how = How.XPATH, using = "//input[@id=\"username\"]")
	public static WebElement userName;

	@FindBy(how = How.XPATH, using = "//input[@id=\"password\"]")
	public static WebElement password;

	@FindBy(how = How.XPATH, using = "//input[@id=\"login_button\"]")
	public static WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//input[@value=\"Approve\"]")
	public static WebElement approveButton;

	@FindBy(how = How.XPATH, using = "//h2[text()=' Success']")
	public static WebElement successMessage;

	public static String getListXapth(String id) {

		return "//a[@href='/list/" + id + "']";
	}

}
