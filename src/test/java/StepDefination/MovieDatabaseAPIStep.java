package StepDefination;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import APIObject.MovieDBAPI;
import DataCenter.MovieDBData;
import PageObject.LoginPage;
import Utility.BaseClass;
import cucumber.api.DataTable;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class MovieDatabaseAPIStep {
	Logger logger = Logger.getLogger("MovieDatabaseAPIStep");
	public static WebDriver driver;
	static String requestToken, accessToken, listID;
	MovieDBAPI movieDBAPI = new MovieDBAPI(driver);;
	MovieDBData movieDBData = new MovieDBData();
	BaseClass baseClass = new BaseClass(driver);
	static Response response;

	@Given("^User Generates the new request Token using POST Call$")
	public void userGenerateRequest() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		requestToken = movieDBAPI.createRequestToken().jsonPath().getString("request_token");
		logger.info("Generated request_token is  " + requestToken);

	}

	@When("^User approves the token by logging in to TMDb website using Username and Password$")
	public void useApprovesToken(DataTable credentials) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		List<List<String>> rows = credentials.asLists(String.class);

		driver = Hooks.driver;

		String approvalUrl = "/auth/access?request_token=" + requestToken;
		baseClass.openBrowser(approvalUrl);

		// Initialization
		PageFactory.initElements(BaseClass.driver, LoginPage.class);

		WebDriverWait wait = (WebDriverWait) new WebDriverWait(BaseClass.driver, 90)
				.ignoring(StaleElementReferenceException.class);
		WebElement element0 = wait.until(ExpectedConditions.visibilityOf(LoginPage.login));
		baseClass.waitForContent();
		baseClass.retryingFindElement(element0);
		baseClass.retryingClick(element0);

		WebElement usernameBox = wait.until(ExpectedConditions.visibilityOf(LoginPage.userName));
		baseClass.waitForContent();
		baseClass.retryingFindElement(usernameBox);
		usernameBox.sendKeys(rows.get(0).get(0));

		WebElement passwordBox = wait.until(ExpectedConditions.visibilityOf(LoginPage.password));
		baseClass.waitForContent();
		baseClass.retryingFindElement(passwordBox);
		passwordBox.sendKeys(rows.get(0).get(1));

		WebElement loginButtonClick = wait.until(ExpectedConditions.visibilityOf(LoginPage.loginButton));
		baseClass.waitForContent();
		baseClass.retryingFindElement(loginButtonClick);
		baseClass.retryingClick(loginButtonClick);

		WebElement approveButton = wait.until(ExpectedConditions.visibilityOf(LoginPage.approveButton));
		baseClass.waitForContent();
		baseClass.retryingFindElement(approveButton);
		baseClass.retryingClick(approveButton);

		WebElement successMsg = wait.until(ExpectedConditions.visibilityOf(LoginPage.successMessage));
		baseClass.waitForContent();
		baseClass.retryingFindElement(successMsg);

		baseClass.tearDown();

		logger.info("User has approved the token");
	}

	@Then("^Generates the access Token using an approved request token over the POST Call$")
	public void generateAccessToken() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		accessToken = movieDBAPI.createAccessToken(requestToken).jsonPath().getString("access_token");
		logger.info("Generated access_token is  " + accessToken);

	}

	@Given("^User is able to create the list using POST call$")
	public void userCreateList() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		response = movieDBAPI.createList(accessToken);
		listID = response.jsonPath().getString("id");
		logger.info("User has created the list using POST call");
	}

	@When("^User validates that list is created using API Response$")
	public void userValidatesList() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		logger.info("created list API response is: " + response.asString());
		listID = response.jsonPath().getString("id");
		Assert.assertEquals("List is not created", 201, response.statusCode());
		Assert.assertNotEquals("list ID is not created", "", response.jsonPath().getString("id"));
		Assert.assertEquals("status_code is not present in API response", "1",
				response.jsonPath().getString("status_code"));
		Assert.assertEquals("status_message is not present in API response",
				"The item/record was created successfully.", response.jsonPath().getString("status_message"));
		Assert.assertEquals("list ID is not created with success message", "true",
				response.jsonPath().getString("success"));

		logger.info("User validates that list is created using API Response");

	}

	@Then("^User should be able to see the created list on list page$")
	public void userVerifiesList() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		driver = Hooks.driver;
		baseClass.openBrowser(movieDBData.listUrl);
		baseClass.waitForContent();
		// Initialization
		PageFactory.initElements(BaseClass.driver, LoginPage.class);

		WebDriverWait wait = (WebDriverWait) new WebDriverWait(BaseClass.driver, 90)
				.ignoring(StaleElementReferenceException.class);
		WebElement list = wait.until(ExpectedConditions
				.visibilityOf(BaseClass.driver.findElement(By.xpath(LoginPage.getListXapth(listID)))));
		baseClass.waitForContent();
		baseClass.retryingFindElement(list);

		baseClass.tearDown();
	}

	@When("^User updates items in list using PUT call$")
	public void userUpdateslist() throws Throwable {

		logger.info("list id: " + MovieDatabaseAPIStep.listID);
		response = movieDBAPI.updateItemsInList(accessToken, listID);

		logger.info("Updated items in list API response is: " + response.asString());
		logger.info("User has updated the items into the created list");
	}

	@Then("^User should be able to see the updated items in list$")
	public void userVerifiesUnderListPage() throws Throwable {

		Assert.assertEquals("Items have not updated successfully", 200, response.statusCode());
		Assert.assertEquals("status code is not present in API response", "1",
				response.jsonPath().getString("status_code"));
		Assert.assertEquals("status_message is not present in API response", "Success.",
				response.jsonPath().getString("status_message"));
		Assert.assertEquals("Items are not updated with success message", "true",
				response.jsonPath().getString("success"));
		logger.info("User has verified items addition in list");

	}

	@When("^User adds the items into the created list using POST call$")
	public void userAddItemsList() throws Throwable {

		logger.info("list id: " + MovieDatabaseAPIStep.listID);
		response = movieDBAPI.addItemsInList(accessToken, listID, movieDBData.payload);

		logger.info("Added items in list API response is: " + response.asString());
		logger.info("User has added the items into the created list");
	}

	@Then("^User should be able to see added items in the list$")
	public void userVerifiesItemsInList() throws Throwable {

		Assert.assertEquals("Items have not added successfully", 200, response.statusCode());
		Assert.assertEquals("status code is not present in API response", "1",
				response.jsonPath().getString("status_code"));
		Assert.assertEquals("status_message is not present in API response", "Success.",
				response.jsonPath().getString("status_message"));
		Assert.assertEquals("Items are not added with success message", "true",
				response.jsonPath().getString("success"));
		logger.info("User has verified items addition in list");

	}

	@When("^User clears the items into the created list using GET call$")
	public void userClearsItemsInList() throws Throwable {

		logger.info("list id: " + MovieDatabaseAPIStep.listID);
		response = movieDBAPI.clearItemsInList(accessToken, listID);

		logger.info("Cleared items form list API response is: " + response.asString());
		logger.info("User has cleared the items from the created list");
	}

	@Then("^User should be able to verify cleared items in the list$")
	public void userVerifiesClearedItemsInList() throws Throwable {

		Assert.assertEquals("List items have not been cleared successfully", 200, response.statusCode());
		Assert.assertEquals("List items have not been cleared successfully", listID,
				response.jsonPath().getString("id"));
		Assert.assertEquals("status code is not present in API response", "1",
				response.jsonPath().getString("status_code"));
		Assert.assertEquals("status_message is not present in API response", "Success.",
				response.jsonPath().getString("status_message"));
		Assert.assertEquals("List items have not been cleared with success message", "true",
				response.jsonPath().getString("success"));
		logger.info("User has verified items addition in list");

	}

	@But("^\\[Negative Tc\\]User should not able to create the list using POST call for incorrect access token$")
	public void userNotAbleToCreateList() throws Throwable {

		response = movieDBAPI.createList(accessToken + "123");

		Assert.assertEquals(401, response.statusCode());
		logger.info("[Negativ Tc]User should not able to create the list using POST call for incorrect access token");
	}

	@But("^\\[Negative Tc\\]User should not able to update the items for unauthorized list$")
	public void userNotAbleToUpdateItemLst() throws Throwable {

		response = movieDBAPI.updateItemsInList(accessToken, movieDBData.unAuthorizedList);
		Assert.assertEquals(401, response.statusCode());
		logger.info("[Negativ Tc]User should not able to update the items for unauthorized list");
	}

	@But("^\\[Negative Tc\\]User should not be able to see added items in the unauthorized list$")
	public void userNotAbleToAddItemLst() throws Throwable {

		response = movieDBAPI.addItemsInList(accessToken, movieDBData.unAuthorizedList, movieDBData.payload);
		Assert.assertEquals(401, response.statusCode());
		logger.info("[Negativ Tc]User should not be able to see added items in the unauthorized list");
	}

	@But("^\\[Negative Tc\\]User should not be able to clear items in the unauthorized list$")
	public void userNotAbleToClearLst() throws Throwable {
		response = movieDBAPI.clearItemsInList(accessToken, movieDBData.unAuthorizedList);
		Assert.assertEquals(401, response.statusCode());
		logger.info("[Negativ Tc]User should not be able to clear items in the unauthorized list");
	}
}
