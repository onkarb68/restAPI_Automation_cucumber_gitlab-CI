package StepDefination;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import APIObject.MovieDBAPI;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.response.Response;

public class Hooks {
	public static WebDriver driver;
	public Properties prop;
	Logger logger = Logger.getLogger("Hooks");
	// Changes in hooks

	@Before
	/**
	 * Delete all cookies at the start of each scenario to avoid shared state
	 * between tests
	 */
	public void openBrowser() throws IOException {
	
	}

	@After()
	public void tearDown(Scenario scenario) throws IOException {

		logger.info("In hook list id: " + MovieDatabaseAPIStep.listID);
		logger.info("In hook acces token is: " + MovieDatabaseAPIStep.accessToken);
		MovieDBAPI movieDBAPI=new MovieDBAPI(driver);
		Response response = movieDBAPI.deleteList(MovieDatabaseAPIStep.accessToken, MovieDatabaseAPIStep.listID);
		
		logger.info("Delete List API response is: " + response.asString());
		
		Assert.assertEquals("list is not deleted", "The item/record was deleted successfully.",
				response.jsonPath().getString("status_message"));

		logger.info("Created list is deleted from hooks");

	}
}
