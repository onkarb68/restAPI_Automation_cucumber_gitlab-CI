package Test_Runner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;

import Utility.FileReaderManager;
import cucumber.api.CucumberOptions;


@RunWith(ExtendedCucumber.class)
@CucumberOptions(features={"Features"},
glue={"StepDefination"},


plugin = { "com.cucumber.listener.ExtentCucumberFormatter:ExtentReports/cucumber-reports/report.html"},
monochrome = true
		 )
public class testrunner {

	@AfterClass
	 public static void writeExtentReport() {
		 
	 Reporter.loadXMLConfig(new File(FileReaderManager.getInstance().getConfigReader().getReportConfigPath()));
	 }

}
