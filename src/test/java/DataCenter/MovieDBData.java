package DataCenter;

import com.google.gson.JsonObject;

import Utility.RestAssuredAPIFramework;

public class MovieDBData {

	public String addItemsInList = "/4/list/";
	public String createRequestTokenURI = "/4/auth/request_token";
	public String createAccessTokenURI = "/4/auth/access_token";
	public String createListURI = "/4/list";
	public String updateListURI = "/4/list";
	public String clearListURI = "/4/list/";
	public String listUrl = "/u/onkarb68/lists";
	public String unAuthorizedList = "7087034";
	RestAssuredAPIFramework restAssuredAPIFramework = new RestAssuredAPIFramework();

	public String payload = "{\r\n" + "  \"items\": [\r\n" + "    {\r\n" + "      \"media_type\": \"movie\",\r\n"
			+ "      \"media_id\": 550\r\n" + "    },\r\n" + "    {\r\n" + "      \"media_type\": \"movie\",\r\n"
			+ "      \"media_id\": 244786\r\n" + "    },\r\n" + "    {\r\n" + "      \"media_type\": \"tv\",\r\n"
			+ "      \"media_id\": 1396\r\n" + "    }\r\n" + "  ]\r\n" + "}";

	public String updateListPayload = "{\r\n" + "  \"items\": [\r\n" + "    {\r\n"
			+ "      \"media_type\": \"movie\",\r\n" + "      \"media_id\": 194662,\r\n"
			+ "      \"comment\": \"Amazing movie!\"\r\n" + "    },\r\n" + "    {\r\n"
			+ "      \"media_type\": \"movie\",\r\n" + "      \"media_id\": 76203,\r\n"
			+ "      \"comment\": \"Wow.\"\r\n" + "    }\r\n" + "  ]\r\n" + "}";

	public String getAddItemsURI(String listId) {
		return addItemsInList + listId + "/items";
	}

	public String deleteListURI(String listId) {
		return restAssuredAPIFramework.getServerAPIEndpoint() + "/4/list/" + listId;
	}

	public JsonObject getPayloadForCreateList() {
		// Create new JSON Object
		JsonObject jsonBody = new JsonObject();
		jsonBody.addProperty("name", "This is the test List created");
		jsonBody.addProperty("iso_639_1", "en");

		return jsonBody;
	}

	public String getUpdateItemsURI(String listId) {
		return addItemsInList + listId + "/items";
	}

	public String getClearItemsURI(String listId) {
		return clearListURI + listId + "/clear";
	}
}
